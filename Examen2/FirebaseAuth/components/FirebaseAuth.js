import { auth, db } from '../FirebaseConfiguration';
import { signInWithEmailAndPassword, createUserWithEmailAndPassword, signOut } from 'firebase/auth';
import { collection, addDoc, getDocs } from 'firebase/firestore';

const FirebaseAuth = {
  signIn: async (email, password) => {
    return signInWithEmailAndPassword(auth, email, password);
  },

  signUp: async (email, password) => {
    const userCredential = await createUserWithEmailAndPassword(auth, email, password);
    await addDoc(collection(db, 'users'), {
      email: email,
      createdAt: new Date(),
    });
    return userCredential;
  },

  signOut: async () => {
    return signOut(auth);
  },

  getUserList: async () => {
    const querySnapshot = await getDocs(collection(db, 'users'));
    return querySnapshot.docs.map(doc => ({ uid: doc.id, ...doc.data() }));
  },
};

export default FirebaseAuth;
