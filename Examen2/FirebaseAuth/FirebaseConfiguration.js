import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';

import { initializeAuth, getReactNativePersistence } from 'firebase/auth';
import ReactNativeAsyncStorage from '@react-native-async-storage/async-storage';




// Configuración de Firebase
const firebaseConfig = {
  apiKey: 'AIzaSyDTStGmQf15ZWpKeod8A8t0g8PHVonD1GE',
  authDomain: 'fir-auth-83afc.firebaseapp.com',
  projectId: 'fir-auth-83afc',
  storageBucket: 'fir-auth-83afc.appspot.com',
  messagingSenderId: '456772904798',
  appId: '1:456772904798:web:f0732ef9068e72c69791cf',
  measurementId: 'G-XY68RG9WJP'
};

// Inicializa Firebase
const app = initializeApp(firebaseConfig);

// Inicializa Auth
const auth = initializeAuth(app, {
  persistence: getReactNativePersistence(ReactNativeAsyncStorage)
});

// Inicializa Firestore
const db = getFirestore(app);

export { auth, db };
